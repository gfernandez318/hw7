'''
Created on May 5, 2014

@author: Joseph Thibodeau and Gustavo Fernandez

@SectionLeads: Logan and Michael
'''
import unittest
import stacking as s

class Test(unittest.TestCase):
    
    def test_init(self):
        self.test_one = s.SSTree(1)
        self.test_two = s.SSTree (12)

    def test_stacks(self):
        self.test_init()
        self.assertEqual("A\nBC", s.SSTree.__repr__(self.test_one)) # print() was throwing an error
        self.assertEqual(["AB"], self.test_one.get_stack_seq_list())
        self.assertEqual(['ABABABABABAB', 'ABABABABABAC', 'ABABABABACAC', 'ABABABABACBC', 'ABABABABCABC', 'ABABABABCBAC', 'ABABABACABAC', 'ABABABACACAC', 'ABABABACACBC', 'ABABABACBABC', 'ABABABACBCBC', 'ABABABCABABC', 'ABABABCABCAC', 'ABABABCABCBC', 'ABABABCACBAC', 'ABABABCBABAC', 'ABABABCBACAC', 'ABABACABABAC', 'ABABACABACAC', 'ABABACABACBC', 'ABABACABCABC', 'ABABACABCBAC', 'ABABACACBABC', 'ABABACACBCAC', 'ABABACACBCBC', 'ABABACBABABC', 'ABABACBABCBC', 'ABABACBACABC', 'ABABACBCACBC', 'ABABCABABCAC', 'ABABCABACABC', 'ABABCABACBAC', 'ABABCABCABAC', 'ABABCABCACBC', "ABABCABCBCAC", "ABABCACABCBC", "ABABCACBABAC", "ABABCACBACBC", "ABABCACBCBAC", "ABABCBABCBAC", "ABABCBACBCAC", "ABACABACABAC", "ABACABACBABC", "ABACABCBACBC", "ABACBACBACBC", "ABACBACBCABC", "ABACBCABACBC", "ABCABCABCABC"], self.test_two.get_stack_seq_list())
