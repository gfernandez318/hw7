'''
Created on Apr 29, 2014

@author: Joseph Thibodeau and Gustavo Fernandez

@SectionLeads: Logan and Michael
'''
import math
import re
import functools


class Node:

    def __init__(self, initialize):  # gustavo
        self.char = initialize
        self.left = None
        self.right = None


class SSTree:

    def __init__(self, n):  # gustavo 
        self.root = Node('A')
        self.height = n
        self._build(self.root, 0)

    def _build(self, node, current_level):  # gustavo
            if current_level < self.height:
                if node.char == 'A':
                    node.left = Node('B')
                    node.right = Node('C')
                elif node.char == 'B':
                    node.left = Node('A')
                    node.right = Node('C')
                elif node.char == 'C':
                    node.left = Node('A')
                    node.right = Node('B')
                self._build(node.left, current_level + 1)
                self._build(node.right, current_level + 1)

    def __repr__(self):  # gustavo
        list1 = []
        for i in range(self.height + 1):
            list1.append("")
        self._repr(self.root, 0, list1)
        return "\n".join(list1)

    def _repr(self, node, level, list2):  # gustavo
        if node.left == None and node.right == None:
            list2[level] = list2[level] + node.char
        else:
            list2[level] = list2[level] + node.char
            self._repr(node.left, level + 1, list2)
            self._repr(node.right, level + 1, list2)

    def get_stack_seq_list(self):  # joe
        tree_list = []

        SSTree._make_stack_seq_list(self.root, "", tree_list)

        SSTree._clean(tree_list)

        return tree_list

    @staticmethod
    def _make_stack_seq_list(root, ss, tree_list):  # joe
        if root.left == None and root.right == None:
            ss = ss + root.char
            tree_list.append(ss)
        else:
            SSTree._make_stack_seq_list(root.right, ss + root.char, tree_list)
            SSTree._make_stack_seq_list(root.left, ss + root.char, tree_list)

    @staticmethod
    def _clean(tree_list):  # joe
        for i in range(len(tree_list) - 1):
            if tree_list[i][-1] == "A":
                tree_list.pop(i)
            else:
                SSTree._clean_perms(tree_list, i)

    @staticmethod
    def _clean_perms(tree_list, index):  # joe
        temp_one = ""
        temp_two = ""
        dict = {}
        for char in tree_list[index]:
            if char not in dict.keys():
                if "A" not in dict:
                    dict[char] = "A"
                elif "B" not in dict:
                    dict[char] = "B"
                else:
                    dict[char] = "C"

        temp_one = tree_list[index].translate(dict)

        for i in range(len(tree_list[index:len(tree_list) - 1])):
            temp_two = tree_list[index + i + 1].translate(dict)
            if temp_one == temp_two:
                tree_list.pop(index + i + 1)

        SSTree._clean_cycles(tree_list, index, tree_list[index])

    @staticmethod
    def _clean_cycles(tree_list, index, seqstring):  # joe
        temp = ""
        for x in range(1):
            for i in range(len(tree_list[index: len(tree_list)-1])):
                temp = tree_list[index + i]
                for j in range(len(temp) + 1):
                    if temp == seqstring:
                        tree_list.pop(index + i)
                        break
                    else:
                        temp = temp[-1] + temp[0:len(temp) - 1]
            seqstring = seqstring[::-1]
